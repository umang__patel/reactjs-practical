import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


const initialState = [];


export const getUserById = createAsyncThunk(
    "users/getUserById",
    async ({ userId }) => {
        const response = await axios.get(`https://dummyjson.com/users/${userId}`, {
            headers: {
                'Content-Type': 'application/json',
            }
        });
        return response;
    }
);

const userSlice = createSlice({
    name: "users",
    initialState,
    extraReducers: {
        [getUserById.fulfilled]: (state, action) => {
            return { ...action.payload };
        },
    },
});

const { reducer } = userSlice;
export default reducer;