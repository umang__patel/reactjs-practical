import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


const initialState = [];

export const getAllPosts = createAsyncThunk(
    "posts/getAllPosts",
    async ({ skip }) => {
        let offset = skip > 0 ? skip : 0;
        const response = await axios.get(`https://dummyjson.com/posts?limit=5&skip=${offset}`, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            }
        });
        return response;
    }
);
export const getPostById = createAsyncThunk(
    "posts/getPostById",
    async ({ postId }) => {
        const response = await axios.get(`https://dummyjson.com/posts/${postId}`, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            }
        });
        return response;
    }
);

const postSlice = createSlice({
    name: "posts",
    initialState,
    extraReducers: {
        [getAllPosts.fulfilled]: (state, action) => {
            return { ...action.payload };
        },
        [getPostById.fulfilled]: (state, action) => {
            return { ...action.payload };
        },
    },
});

const { reducer } = postSlice;
export default reducer;