import { configureStore } from '@reduxjs/toolkit'
import postReducer from '../slice/post';
import userReducer from '../slice/user';

const reducer = {
    posts: postReducer,
    users: userReducer
}

const store = configureStore({
    reducer: reducer,
    devTools: true,
})

export default store;