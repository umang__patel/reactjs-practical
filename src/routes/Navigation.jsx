import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Post from '../components/Post'
import Posts from '../components/Posts'

const Navigation = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path={'/'} element={<Posts />} />
                <Route path={'/post/:postId'} element={<Post />} />
                <Route path={'*'} element={<h1>Page Not Found</h1>} />
            </Routes>
        </BrowserRouter>
    )
}

export default Navigation
