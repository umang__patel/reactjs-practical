import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { getAllPosts } from '../slice/post'
import Post from './Post';

const Posts = () => {

    const [posts, setPosts] = useState([])
    const [total, setTotal] = useState(0)
    const dispatch = useDispatch();

    const getPageNumber = (e) => {
        let skip = e * 5;
        const data = dispatch(getAllPosts({ skip }));
        data.then((res) => {
            setPosts(res?.payload?.data?.posts)
        });
    }
    const createPagination = (n) => {
        let elements = [];
        for (let index = 1; index <= n; index++) {
            elements.push(<li className="page-item" onClick={(e) => { getPageNumber(index) }}><p className="page-link" style={{ cursor: 'pointer' }}>{index}</p></li>);
        }
        return elements;
    }
    useEffect(() => {
        let skip = 0
        const data = dispatch(getAllPosts({ skip }));
        data.then((res) => {
            let total = res?.payload?.data?.total;
            let pages = total % 5 === 0 ? (total / 5) - 1 : parseInt(total / 5)
            setTotal(pages)
            setPosts(res?.payload?.data?.posts)
        });
    }, [dispatch]);
    return (
        <div className='container'>
            <h1 className='text-center mt-3'>Posts</h1>
            <div className="row mt-3">
                {posts?.length > 0 && <Post postProps={posts} />}
            </div>

            <div className="row">
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        {createPagination(total)}
                    </ul>
                </nav>
            </div>

            <div className='bg-primary text-white text-center p-2 w-100 m-0'>
                ReactJs B2E Practical Test - Umang Patel
            </div>
        </div >
    )
}



export default Posts
