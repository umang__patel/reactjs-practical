import React from 'react'

const PopUp = ({ closePopUp, user }) => {
    const popupClose = () => {
        closePopUp()
    };
    return (
        <div className="popup-box">
            <div className="box">
                <span className="close-icon" onClick={popupClose}>
                    x
                </span>

                <p className="text-center">Name : {user.firstName + '' + user.lastName}</p>
                <p className="text-center">Username : {user.username}</p>
                <p className="text-center">Email : {user.email}</p>
                <p className="text-center">University : {user.university}</p>
                <p className="text-center">BirthDate : {user.birthDate}</p>

            </div>
        </div>
    )
}

export default PopUp
