import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { getPostById } from '../slice/post'
import { getUserById } from '../slice/user'
import PopUp from './PopUp'

const Post = ({ postProps }) => {
    const [isOpen, setIsOpen] = useState(false)
    const [post, setPost] = useState()
    const [user, setUser] = useState()
    const { postId } = useParams()
    const dispatch = useDispatch();
    const navigate = useNavigate()

    useEffect(() => {
        setPost(postProps)
    }, [postProps])

    const openPopUp = () => {
        setIsOpen(true)
    }

    const closePopUp = () => {
        setIsOpen(false)
    }

    useEffect(() => {
        if (postId) {
            const data = dispatch(getPostById({ postId }));
            data.then((d) => {
                if (!d?.payload?.data) {
                    navigate('/')
                }
                else {
                    setPost([d?.payload?.data])
                    let userId = d?.payload?.data.userId
                    const userData = dispatch(getUserById({ userId }));
                    userData.then((userData) => {
                        setUser(userData?.payload?.data);
                    })
                }
            })
        }
    }, [postId, dispatch, navigate])

    return (
        <div className={`row ${post?.length === 1} ? 'd-flex justify-content-center align-items-center' : ''`}>

            <div className='d-flex justify-content-end p-3'>
                {user && <button className='btn btn-primary'><Link className='text-white' to={`/`}>Home</Link></button>}
            </div>
            {post?.map((post) => {
                return (
                    <div className={`col-sm-4 p-3`} key={post.id}>
                        <Link to={`/post/${post.id}`} style={{ textDecoration: 'none', color: '#363738' }}>
                            <div className="card">
                                <div className="card-body" style={{ height: '350px', overflowX: 'auto' }}>
                                    <h5 className="card-title">{post.title}</h5>
                                    <p className="card-text">{post.body}</p>
                                    Tags:
                                    {post?.tags.map((tag) => {
                                        return (<span key={tag} style={{ fontSize: '15px' }} className='badge text-primary'>{tag}</span>)
                                    })}

                                    {user && <p onClick={openPopUp} style={{ fontSize: '18px', cursor: 'pointer', marginTop: '40px', padding: '10px', border: '1px solid rgb(140, 136, 136)' }}>Author Name : {user?.firstName + ' ' + user?.lastName}</p>}
                                </div>
                            </div>
                        </Link>
                        {isOpen && user && <PopUp closePopUp={closePopUp} user={user} />}
                    </div>
                )
            })}
        </div>

    )
}

export default Post
